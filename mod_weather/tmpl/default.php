<?php
defined('_JEXEC') or die('(@)|(@)');

$curntom = json_decode($list['current']->value, true);
$daily = json_decode($list['sixteen_day']->value, true);

$tomorrow = date("d-m-y", strtotime("+1 day"));

?>


<a class="wr_tommorow" href="/index.php?option=com_weather">
    <div class="wr_cond">
        <span class="wr_date">завтра</span>
        <div class="wr_data"><?= round($daily[$tomorrow]['temp']['day'], 1); ?>&ordm;C</div>
    </div>
    <div class="<?= 'fr_b' . $daily[$tomorrow]['weather'][0]['icon'] ?> wr_icon">
        <img src="/images/weather/module/<?= $daily[$tomorrow]['weather'][0]['icon'] ?>.png" />
    </div>
</a>
<a class="wr_today" href="/index.php?option=com_weather" title="<?= date('H', $curntom['dt']) - 1;
echo date(':i', $curntom['dt']); ?>">
    <div class="wr_cond">
        <span class="wr_date">зараз</span>
        <div class="wr_data"><?php echo round($curntom['main']['temp'], 1); ?>&ordm;C</div>
    </div>
    <div class="fr_b<?= $curntom['weather'][0]['icon'] ?> wr_icon">
        <img src="/images/weather/module/<?= $curntom['weather'][0]['icon']; ?>.png" />
    </div>
</a>