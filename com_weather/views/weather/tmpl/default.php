<?php
/**
 * Author:	Liubomyr Bondarchuk
 * Email:	Bondarczuk@ukr.net
 * Component:Weather
 * Version:	1.0.0
 * Date:		01/09/2015
 * copyright	Copyright (C) 2015 All Rights Reserved.
 * @license	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * */
// no direct access
defined('_JEXEC') or die;

$weather = $this->fiveDayforecast;
$daily = $this->sixteenDayforecast;

$ti1me = strtotime("" . date("Y") . "-" . $month . "-" . $day . ", -1 day");
$time = strtotime($weather[0]['dt_txt']);


if (isset($_GET['day'])) {
    $forday = $_GET['day'];
    if ($forday == '' || $forday > 5 || $forday < 1) {
        unset($forday);
    }
}
?>
<!-- Weather 1.0.0 starts here -->

<div style="overflow:hidden;position:relative;font-size:15px;padding-top:5px;text-align:left;">
    <div id="wf1" class="<?= (($forday == 1 || !isset($forday)) ? 'wfblcok' : 'wfnone'); ?>">
        <div class="wf_day">
                <span style="font-size:22px;font-weight:bold;color:#505050;">
                    <?= JHtml::_('date', $daily[date('d-m-y', $weather[0]['dt'])]['dt'], JText::_('l')) ?></br>
                    <?= JHtml::_('date', $daily[date('d-m-y', $weather[0]['dt'])]['dt'], JText::_('j F')) ?>
                </span>
            <div class="<?= $this->getIconClass($daily[date('d-m-y', $weather[0]['dt'])]['weather'][0]['icon']) ?>">
                <img src="/images/weather/component/<?= $daily[date('d-m-y', $weather[0]['dt'])]['weather'][0]['icon']; ?>.png" />
            </div>
            <div class="tempmax"><?= round($daily[date('d-m-y', $weather[0]['dt'])]['temp']['max'], 1) ?>&ordm;</div>
            <div class="tempmin"><?= round($daily[date('d-m-y', $weather[0]['dt'])]['temp']['min'], 1) ?>&ordm;</div>
        </div>
        
        <div class="wf_daytime">
        <?php for ($i = 0; $i <= 7; $i++) : ?>
            <div class="wftime">
                <span style="display:block;">
                    <?php if (date("d", $weather[$i]['dt']) != date("d", $weather[0]['dt'])) : ?>
                        <span style="color:#929292;">
                            <?= JHtml::_('date', $weather[$i]['dt'], JText::_('D H:i'), false) ?>
                        </span>
                    <?php else : ?>
                        <?= JHtml::_('date', $weather[$i]['dt'], JText::_('H:i'), false) ?></br>
                    <?php endif; ?>
                </span>
                <span style="display:block;"><?= $weather[$i]['weather'][0]['description'] ?></span>
                <div class="wi<?= $weather[$i]['weather'][0]['icon'] ?>">
                    <img src="/images/weather/component/<?= $weather[$i]['weather'][0]['icon'] ?>.png" />
                </div>
                <span class="temperature"><?= round($weather[$i]['main']['temp'] . '', 1) ?>&ordm;</span>
                <span style="display:block;">вітер: <?= round($weather[$i]['wind']['speed'], 1) ?> м/с <?= $this->printWindDirection($weather[$i]['wind']['deg']) ?></span>
                <span style="display:block;">тиск: <?= round($weather[$i]['main']['pressure'] * 0.75008, 0) ?> мм</span>
                <span style="display:block;">хмарність: <?= $weather[$i]['clouds']['all'] ?>%</span>
                <span style="display:block;">вологість: <?= $weather[$i]['main']['humidity'] ?>%</span>
            </div>
        <?php endfor; ?>
        </div>
    </div>

    <?php
    $currentDay = $this->getElementSecondDayBegins(date("H", $weather[0]['dt'])) + 1;

    for ($d = 0; $d <= 3; $d++) :
        ?>
        <div class="wfblock">
            <div class="wf_day">
                    <span style="font-size:22px;font-weight:bold;color:#505050;">
                        <?= JHtml::_('date', $daily[date('d-m-y', $weather[$currentDay]['dt'])]['dt'], JText::_('l')) ?></br>
                        <?= JHtml::_('date', $daily[date('d-m-y', $weather[$currentDay]['dt'])]['dt'], JText::_('j F')) ?>
                    </span>
                <div class="<?= $this->getIconClass($daily[date('d-m-y', $weather[$currentDay]['dt'])]['weather'][0]['icon']) ?>">
                    <img src="/images/weather/component/<?= $daily[date('d-m-y', $weather[$currentDay]['dt'])]['weather'][0]['icon']; ?>.png" />
                </div>
                <div class="tempmax"><?php echo round($daily[date('d-m-y', $weather[$currentDay]['dt'])]['temp']['max'], 1); ?>&ordm;</div>
                <div class="tempmin"><?php echo round($daily[date('d-m-y', $weather[$currentDay]['dt'])]['temp']['min'], 1); ?>&ordm;</div>
            </div>

            <div class="wf_daytime">
            <?php for ($i = 0; $i <= 7; $i++) : ?>
                <div class="wftime">
                    <span style="display:block;">
                        <?= JHtml::_('date', $weather[$currentDay]['dt'], JText::_('H:i')) ?></br>
                    </span>
                    <span style="display:block;"><?= $weather[$currentDay]['weather'][0]['description'] ?></span>
                    <div class="wi<?= $weather[$currentDay]['weather'][0]['icon'] ?>">
                        <img src="/images/weather/component/<?= $weather[$currentDay]['weather'][0]['icon'] ?>.png" />
                    </div>
                    <span class="temperature"><?= round($weather[$currentDay]['main']['temp'] . '', 1) ?>&ordm;</span>
                    <span style="display:block;">вітер: <?= round($weather[$currentDay]['wind']['speed'], 1) ?> м/с <?= $this->printWindDirection($weather[$currentDay]['wind']['deg']) ?></span>
                    <span style="display:block;">тиск: <?= round($weather[$currentDay]['main']['pressure'] * 0.75008, 0) ?> мм</span>
                    <span style="display:block;">хмарність: <?= $weather[$currentDay]['clouds']['all'] ?>%</span>
                    <span style="display:block;">вологість: <?= $weather[$currentDay]['main']['humidity'] ?>%</span>
                </div>
                <?php $currentDay++;
            endfor;
            ?>
            </div>
        </div>
    <?php endfor; ?>
        
</div>

<!-- Weather 1.0.0 ends here -->
