<?php
/**
* Author:	Liubomyr Bondarchuk
* Email:	Bondarczuk@ukr.net
* Component:Weather
* Version:	1.0.0
* Date:		01/09/2015
* copyright	Copyright (C) 2015 All Rights Reserved.
* @license	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
**/
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class WeatherViewWeather extends JViewLegacy
{
    protected $current;
	protected $fiveDay;
	protected $sixteenDay;

	/**
	 * Display the Hello World view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{
		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JLog::add(implode('<br />', $errors), JLog::WARNING, 'jerror');

			return false;
		}

		// Display the view
		parent::display($tpl);
	}

	public function printWindDirection($degree)
	{
	    if($degree<(0+(45/2))) return '<span class="wind-direction" title="Північний">&#8595;</span>';
	    if($degree<(45+(45/2)) && $degree>(45-(45/2))) return '<span class="wind-direction" title="Північно-східний">&#8601;</span>';
	    if($degree<(90+(45/2)) && $degree>(90-(45/2))) return '<span class="wind-direction" title="Східний">&#8592;</span>';
	    if($degree<(135+(45/2)) && $degree>(135-(45/2))) return '<span class="wind-direction" title="Південно-східний">&#8598;</span>';
	    if($degree<(180+(45/2)) && $degree>(180-(45/2))) return '<span class="wind-direction" title="Південний">&#8593;</span>';
	    if($degree<(225+(45/2)) && $degree>(225-(45/2))) return '<span class="wind-direction" title="Південно-західний">&#8599;</span>';
	    if($degree<(270+(45/2)) && $degree>(270-(45/2))) return '<span class="wind-direction" title="Західний">&#8594;</span>';
	    if($degree<(315+(45/2)) && $degree>(315-(45/2))) return '<span class="wind-direction" title="Північно-західний">&#8600;</span>';
	    if($degree>(360-(45/2))) return '<span class="wind-direction" title="Північний">&#8595;</span>';
	}

	public function getElementSecondDayBegins($todaysCurrentTime)
	{
            if($todaysCurrentTime=='00') return 7;
            if($todaysCurrentTime=='03') return 6;
            if($todaysCurrentTime=='06') return 5;
            if($todaysCurrentTime=='09') return 4;
            if($todaysCurrentTime=='12') return 3;
            if($todaysCurrentTime=='15') return 2;
            if($todaysCurrentTime=='18') return 1;
            if($todaysCurrentTime=='21') return 0;
	}

	public function getIconClass($iconFileName)
	{
            $bicnCssClassIcons = ['09d', '09n', '10d', '10n', '11d', '11n', '13d', '13n'];
            if(in_array($iconFileName, $bicnCssClassIcons)){
                return 'bicn';
            } else {
                return 'sicn';
            }
	}
}
