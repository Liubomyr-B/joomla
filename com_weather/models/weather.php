<?php
/**
* Author:	Liubomyr Bondarchuk
* Email:	Bondarczuk@ukr.net
* Component:Weather
* Version:	1.0.0
* Date:		01/09/2015
* copyright	Copyright (C) 2015 All Rights Reserved.
* @license	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL   
 */

defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');	

/**
 * Weather Component Model
 *
 * @since  1.0
 */
class WeatherModelWeather extends JModelItem
{
	const CURRENT_WEATHER = 'current';
	const FIVE_DAY_FORECAST = 'five_day';
	const SIXTEEN_DAY_FORECAST = 'sixteen_day';

	private $current;
	private $fiveDay;
	private $sixteenDay;

	protected $tableName = '#__weather2';

    public function update($term, $json)
    {
    	$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		// Fields to update.
		$fields = array(
		    $db->quoteName('value') . ' = ' . $db->quote($json)
		);

		// Conditions for which records should be updated.
		$conditions = array(
		    $db->quoteName('term') . ' = ' .  $db->quote($term)
		);

		$query->update($db->quoteName($this->tableName))->set($fields)->where($conditions);

		$db->setQuery($query);

		$result = $db->execute();

		return $result;
    }

    public function loadAll()
    {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from($db->quoteName($this->tableName));

		// Reset the query using our newly populated query object.
		$db->setQuery($query);
		$results = $db->loadObjectList('term');
		$this->current = json_decode($results[self::CURRENT_WEATHER]->value, true); 
		$this->fiveDay = json_decode($results[self::FIVE_DAY_FORECAST]->value, true); 
		$this->sixteenDay = json_decode($results[self::SIXTEEN_DAY_FORECAST]->value, true); 
    }

    public function getCurrentWeather()
    {
    	return $this->current;
    }

    public function getFiveDayForecast()
    {
    	return $this->fiveDay;
    }

    public function getSixteenDayForecast()
    {
    	return $this->sixteenDay;
    }
}