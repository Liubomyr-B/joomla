<?php
/**
* Author:	Liubomyr Bondarchuk
* Email:	Bondarczuk@ukr.net
* Component:Weather
* Version:	1.0.0
* Date:		01/09/2015
* copyright	Copyright (C) 2015 All Rights Reserved.
* @license	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
**/
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

class WeatherController extends JControllerLegacy
{

	public function display($cachable = false, $urlparams = false)
	{
		//JRequest::setVar('model','weather'); // force it to be the search view
		//return parent::display($cachable, $urlparams);

 		$model = $this->getModel('weather');
 		$model->loadAll();
 		
		$view = $this->getView('Weather','html', 'WeatherView');
		//print_r($view);die;
		$view->currentWeather = $model->getCurrentWeather();

		$view->fiveDayforecast = $model->getFiveDayForecast();
		$view->sixteenDayforecast = $model->getSixteenDayForecast();
		$view->display();
	}

	public function update()
	{
		$result = [];

		$result['current'] = $this->updateCurrentWeather();
		$result['fiveDay'] = $this->updateFiveDayForecast();
		$result['sixteenDay'] = $this->updateSixteenDayForecast();

		echo json_encode($result);
	}

	public function updateCurrentWeather()
	{
	    $json = @file_get_contents('http://api.openweathermap.org/data/2.5/weather?id=698232&units=metric&lang=uk&APPID=17e2d413ced813345989b1963ec8efc2');
	    
	    if($json == false) return false;

	    $model = $this->getModel('weather');
		return $model->update($model::CURRENT_WEATHER, $json);
	}

	public function updateFiveDayForecast()
	{
	    $json = @file_get_contents('http://api.openweathermap.org/data/2.5/forecast?id=698232&units=metric&lang=uk&APPID=17e2d413ced813345989b1963ec8efc2');
	    
	    if($json == false) return false;
	    $sourceData = json_decode($json, true);
	    $weatherData = json_encode($sourceData['list'], JSON_UNESCAPED_UNICODE);

	    $model = $this->getModel('weather');
		return $model->update($model::FIVE_DAY_FORECAST, $weatherData);
	}

	public function updateSixteenDayForecast()
	{
	    $json = @file_get_contents('http://api.openweathermap.org/data/2.5/forecast/daily?id=698232&units=metric&lang=uk&APPID=17e2d413ced813345989b1963ec8efc2');
	    
	    if($json == false) return false;
	    $sourceData = json_decode($json, true);

	   	$weatherData = [];
	    foreach($sourceData['list'] as $item) {
	    	$weatherData[date('d-m-y', $item['dt'])] = $item;
	    }

	    $model = $this->getModel('weather');
		return $model->update($model::SIXTEEN_DAY_FORECAST, json_encode($weatherData, JSON_UNESCAPED_UNICODE));
	}

}
