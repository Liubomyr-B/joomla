<?php
defined('_JEXEC') or die('Access Deny');
class plgContentImagecompressor extends JPlugin
{
public function onContentBeforeSave($context, $article, $isNew)
{

$image = json_decode($article->images);
$introimage = $image->image_intro;

if(!empty($introimage)){						
	$image_path = pathinfo($introimage);
	if (!is_dir(JPATH_ROOT . DS . $image_path['dirname'] . DS . 'thumbs')) {
		mkdir(JPATH_ROOT . DS . $image_path['dirname'] . DS . 'thumbs', 0777, true);
	}
	$thumb_path = JPATH_ROOT . DS . $image_path['dirname'] . DS . 'thumbs' . DS . $image_path['filename'].'_'.'thumb'.'.'.$image_path['extension'];
	$ftrd_path = JPATH_ROOT . DS . $image_path['dirname'] . DS . 'thumbs' . DS . $image_path['filename'].'_'.'big'.'.'.$image_path['extension'];
	$ftrd_path2 = JPATH_ROOT . DS . $image_path['dirname'] . DS . 'thumbs' . DS . $image_path['filename'].'_'.'small'.'.'.$image_path['extension'];
						
	jimport('joomla.image.image');
	
	$img = new JImage(JPATH_ROOT . DS . $introimage);
	$img2 = new JImage(JPATH_ROOT . DS . $introimage);
	if($article->featured==1){
		$img->cropResize(415, 312, false);
		$img->toFile($ftrd_path, IMAGETYPE_JPEG, array('quality' => '90'));
		$img2->cropResize(245, 163, false);
		$img2->toFile($ftrd_path2, IMAGETYPE_JPEG, array('quality' => '90'));
	}
	
	$thumbimg = new JImage(JPATH_ROOT . DS . $introimage);
	$thumbimg->cropResize(200, 140, false);
	$thumbimg->toFile($thumb_path, IMAGETYPE_JPEG, array('quality' => '90'));
	
}

}

}